### Counting Inversions ###

def readarray(filename):

    ### read the file and put the numbers in a list ###

    f = open(filename, 'r')
    list_of_integers = []
    for line in f:
        line = int(line)
        list_of_integers.append(line)
    return list_of_integers

def countSplitInv(first_half, second_half):
    
    ### count number of split inversions ###
    countinv = 0
    while len(first_half) > 0 and len(second_half) > 0:
            if first_half[0] < second_half[0]:
                first_half.pop(0)
            else:
                second_half.pop(0)
                countinv += len(first_half)
    return countinv

def sort_and_count_inversions(list_of_integers):

    ### read the list of intgers and count inversions ###

    if(len(list_of_integers) == 1):
        return 0
    else:
        first_half = list_of_integers[:len(list_of_integers)/2]
        second_half = list_of_integers[len(list_of_integers)/2:]
        x = sort_and_count_inversions(first_half)
        y = sort_and_count_inversions(second_half)
        z = countSplitInv(sorted(first_half), sorted(second_half))
        return x+y+z


list_of_integers = readarray("IntegerArray.txt")
count_inversions = sort_and_count_inversions(list_of_integers)
print count_inversions
