# insertion sort

def insertion_sort(A, reverse=False):
    for j in range(1, len(A)):
        # insert A[i] into sorted sequence A[0]...A[i-1]
        i = j-1
        key = A[j]
        while A[i] > key and i > -1:
            A[i+1] = A[i]
            i = i-1
        A[i+1] = key
    if reverse == True:
         A.reverse()
    return A

A = [6, 1, 3, 16, 4]
ans = insertion_sort(A)
print ans
ans = insertion_sort(A, reverse=True)
print ans
